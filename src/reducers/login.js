export default function(state = null, action) {
    switch(action.type) {
        case 'user_login':
            return {...state, login_data:action.payload, success: true};

        case 'user_login_failure':
                return {...state, login_data:[], success: false};    
        }
    return state
}