import {
    USER_LOGIN,
    USER_LOGIN_FAILURE,
    USER_SIGNUP,
    USER_FORGOT
} from './type';

import history from '../history';

export const userSignIn = (data, props) => {
    console.log(history);
    
    if(data.username.includes('@')) {
         props.history.push('/signup');
        
        return {
            type: USER_LOGIN,
            payload: data
        }
    } else {
        return {
            type: USER_LOGIN_FAILURE
                }
    }
    
    // axios.post(`url`, { username, password }, {
    //     headers: { 'Authorization': 'TOKEN' }
    //   })
    //   .then(res => {
    //     console.log(res);
    //     console.log(res.data);
    //     res.data checks here
    //   })
    //   .catch(err => {
            // dispatch({
            //     type: USER_LOGIN_FAILURE,
            //     payload: data
            // })
    //       console.log(err);
    //   });
}