export const USER_LOGIN = 'user_login';
export const USER_LOGIN_FAILURE = 'user_login_failure';
export const USER_SIGNUP = 'user_signup';
export const USER_FORGOT = 'user_forgot';
