import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import history from './history';

import App from './components/app';
import Login from './components/login/login';
import Signup from './components/register/register';
import Forgot from './components/forgot/forgot';
import reducers from './reducers';

const createStoreWithMiddleware = applyMiddleware()(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <Router history={history}>
      <Route path="/" exact component={Login} />
      <Route path="/app" component={App} />
      <Route path="/signup" component={Signup} />
      <Route path="/forgot" component={Forgot} />
    </Router>
  </Provider>
  , document.querySelector('.container'));
