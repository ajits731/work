import React, { Component } from 'react';
import {Link} from 'react-router-dom';

class Forgot extends Component {
  render() {
    return (
      <div className="main">
          <div className="test">
          <div className="wrap">
          <form>
              <h3>Please enter your email</h3>
              <table className="table">
                    <tbody>
                    <tr>
                        <td>Email:</td>
                        <td><input type="email" alt="email" /></td>
                    </tr>
                    </tbody>
                </table>
                <div className="btn-groups">
                    <button className="btn btn-primary">Submit</button>       
                    <Link to="/">Sign in</Link>
                </div>
                
          </form>
          </div>
          </div>
      </div>
    );
  }
}

export default Forgot;