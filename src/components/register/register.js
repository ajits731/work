import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import PropTypes from "prop-types";
import { withRouter } from "react-router";

class Register extends Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  };
  render() {
    return (
      <div className="main">
          <div className="test">
          <div className="wrap">
          <form>
              <h3>Please enter your credentials</h3>
              <table className="table">
                    <tbody>
                    <tr>
                        <td>Email:</td>
                        <td><input type="email" alt="email" /></td>
                    </tr>
                    <tr>
                        <td>Password: </td>
                        <td><input type="password" alt="email" /></td>
                    </tr>
                    <tr>
                    <td>Confirm Password:</td>
                        <td><input type="password" alt="email" /></td>
                    </tr>
                    </tbody>
                </table>
                <div className="btn-groups">
                    <button className="btn btn-primary">Submit</button>
                    <Link to="/">Sign in</Link> 
                </div>
                
          </form>
          </div>
          </div>
          
      </div>
    );
  }
}

const RegisterWithRouter = withRouter(Register);

export default RegisterWithRouter;