import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import {userSignIn} from '../../actions/index';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import history from '../../history';
import PropTypes from "prop-types";
import { withRouter } from "react-router";


class Login extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    static propTypes = {
        match: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired
      };

    handleSubmit = () => {
        console.log('this.refs:',this.refs);
        this.props.userSignIn({username:this.refs.user_email.value, password:this.refs.user_pass.value}, this.props);
    }
  render() {
    return (
      <div className="main">
          <div className="test">
            <div className="wrap">
            <form>
              <h4>Please enter your credentials</h4>
              <table className="table">
                    <tbody>
                    <tr>
                        <td>Email:</td>
                        <td><input type="email" ref="user_email"  alt="email" /></td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td><input ref="user_pass" type="password" alt="email" /></td>
                    </tr>
                    </tbody>
                </table>
          </form>
            <div className="btn-groups">
                    <button onClick={this.handleSubmit} className="btn btn-primary">Submit</button>       
                    <Link to="signup">Sign Up</Link>
                    <Link to="forgot">Forgot Password?</Link>
            </div>
            {this.props.login_data && !this.props.login_data.success && <p>Opps! API didn't</p>}
            </div>    
          </div>
      </div>
    );
  }
}

const LoginWithRouter = withRouter(Login);

const mapStateToProps = (state) => {
    console.log(state.login);
    return {
      login_data: state.login
    }
  }

const mapDispatchToProps = dispatch => bindActionCreators({
    userSignIn
  },dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(LoginWithRouter);